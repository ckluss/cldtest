#' cldtest
#'
#' compact letter display
#'
#' @docType package
#' @name cldtest
#' @import grid purrr scales ggplot2
#'  XML curl RCurl magrittr multcomp igraph stringr sqldf nlme lattice
#' latticeExtra directlabels mratios geosphere RSQLite
#' gdata RColorBrewer openxlsx ReporteRs car tidyr dplyr lubridate
#' hydroGOF readr readxl gridExtra piecewiseSEM sfsmisc minpack.lm tools
NULL


#' construct a diagonal matrix
#'
#' @param x vector
#'
#' @return diagonal matrix
#' @export
diagFac <- function(x) {
  mat <- diag(nlevels(x))
  colnames(mat) <- rownames(mat) <- levels(x)
  mat
}

#' rowMat
#'
#' @param x
#'
#' @return matrix
#' @export
rowMat <- function(x) {
  mat <- rbind(rep(1 / nlevels(x), nlevels(x)))
  colnames(mat) <- levels(x)
  mat
}

#' generalised kronecker product
#'
#' @param x
#' @param y
#' @param ...
#'
#' @return generalised kronecker product
#' @export
#'
# @examples
kroneckerT <- function(x,y,...) {
  if (nrow(x) == 0)
    return(y)
  if (nrow(y) == 0)
    return(x)
  kronecker(x,y,make.dimnames = TRUE, ...)
}

#' Symbolic Number Coding
#'
#' @param x numeric vector

#'
#' @return Symbolically encoding
#' @export
symp <- function(x) {
  as.character(symnum(
    x, cutpoints = c(0,0.001,0.01,0.05,0.1,1),
    symbols = c("***","**","*","."," ")
  ))
}

#' perform replacement of colons
#'
#' @param x
#'
#' @return replacement
#' @export
delColons <- function(x) {
  x <- gsub("\\:+",":",x)
  gsub("^:|:$","",x)
}


#' clddisplay2
#'
#' @param sigdata
#' @param vergl
#' @param colnames
#' @param data
#' @param sep
#' @param pval
#' @param alles
#' @param ord
#'
#' @return clddisplay2
#' @export
clddisplay2 <- function(sigdata, vergl, colnames, data,
                        sep = ":", pval = 0.05, alles = FALSE, ord = TRUE) {
  f <- function(x) {
    unique(`[[` (data,x))
  }

  l <- lapply(colnames, f)
  d <- expand.grid(l) %>% setNames(colnames)
  d[[vergl]] <- ".*"
  d %<>%
    mutate_all(funs(as.character)) %>%
    distinct %>% tbl_df

  res <- data_frame()
  sigdata %<>% mutate(test = delColons(test))
  # nsigdata %>% select(-test)

  for (i in 1:nrow(d)) {
    t <- if (length(l) == 1) { paste(d[i,], collapse = "")
    } else { paste(d[i,], collapse = ":") }

    dat2 <- sigdata %>% filter(grepl(t, test, perl = TRUE))
    if (nrow(dat2) > 0) {
      m <- clddisplay(dat2, pval, ord = ord)
      if (!all(m$label == "a") | alles) {
        res %<>%
          rbind(clddisplay(dat2, pval, ord = ord))  %>%
          rbind(data_frame(names = c("-"),label = c("-")))
      }
    }
  }
  res
}


#' clddisplay
#'
#' @param data
#' @param pval
#' @param ord
#'
#' @return clddisplay2
#' @export
clddisplay <- function(data, pval = 0.05, ord = TRUE) {
  # Set up a compact letter display of all pair-wise comparisons
  with(data, stopifnot(!any(is.na(
    c(name1, name2, pvalue)
  ))))

  ns1 <- as.character(data$name1)
  ns2 <- as.character(data$name2)
  ns  <- sort(unique(c(ns1, ns2)))
  A   <- matrix(TRUE, nrow=length(ns), ncol=length(ns), dimnames=list(ns,ns))
  for (i in seq_along(data$pvalue))
    A[ns1[i],ns2[i]] <- A[ns2[i],ns1[i]] <- (data$pvalue[i] > pval)
  g <- as.matrix(A)
  cliq <- maximal.cliques(graph.adjacency(g, mode = "undirected"))
  # Reorder by level order
  ml <- max(sapply(cliq, length))
  sf <- function(x)
    sort(x)[seq_len(ml)]
  # data_frame ???
  if (ord) {
    cliq2 <- lapply(cliq, as.numeric)
    reord <- do.call(order, as.data.frame(do.call(rbind, lapply(cliq2, sf))))
    cliq <- cliq[reord]
  }

  labs <- rep("", nrow(g))
  lab  <- letters[seq_along(cliq)]
  for (i in seq_along(cliq))
    for (j in cliq[[i]])
      labs[[j]] <- paste0(labs[[j]], lab[i])
  #if (length(lab) > 9)
  #  labs <- rep("",length(ns))
  data_frame(names = ns, label = labs)
}

#' as_data_frame.glht
#'
#' @param obj
#'
#' @return as_data_frame.glht
#' @export
as_data_frame.glht <- function(obj) {
  names <- rownames(obj$linfct) %>% delColons
  help  <- str_split_fixed(names, " - ", 2)
  name1 <- name2 <- vector(mode = "numeric",length = nrow(help))
  for (i in 1:nrow(help)) {
    s1 <- unlist(str_split(help[i, 1], ":"))
    s2 <- unlist(str_split(help[i, 2], ":"))
    s1end <- tail(s1, n = 1)
    s1beg <- if (length(s1) > 1) { s1[1:(length(s1) - 1)] } else { character(0) }
    s2beg <- head(s2, n = 1)
    s2end <- if (length(s2) > 1) { s2[-1] } else  { character(0) }
    name1[i] <- paste(c(s1beg, s1end, s2end), collapse = ":")
    name2[i] <- paste(c(s1beg, s2beg, s2end), collapse = ":")
  }
  s <- summary(obj)$test
  data_frame(
    test = names, name1, name2, estimate = s$coefficients,
    pvalue = s$pvalues, sigma = s$sigma, tstat = s$tstat,
    symbol = symp(s$pvalues)
  )
}

#' colGroupMeans
#'
#' @param data
#' @param colnames
#' @param Y
#' @param add
#' @param sep
#'
#' @return colGroupMeans
#' @export
colGroupMeans <- function(data, colnames, Y, add = NULL,sep = ":") {
  sa <- paste(colnames, collapse = ", ")
  sb <- paste(colnames, collapse = paste0(" || '", sep, "' || "))
  if (is.null(add)) {
    sqlstr <- paste("select",sb,"as names, avg(",Y,") as means",
                    "from data group by",sa)
  } else {
    sg <- paste(add, collapse = paste0(" || '", sep, "' || "))
    if (sg == "") { sg <- sa }
    sqlstr <- paste(
      "select", sg ,"as grpname,", sb,"as names,
      avg(",Y,") as means", "from data group by",sa
    )
  }
  sqldf(sqlstr)
}

#' means.glht
#'
#' @param obj
#' @param data
#' @param colnames
#' @param Y
#'
#' @return means.glht
#' @export
means.glht <- function(obj, data, colnames, Y) {
  stopifnot(class(obj) == "glht")
  means <- colGroupMeans(data, colnames, Y)
  dat_obj <- as_data_frame.glht(obj)

  dat_obj %>%
    inner_join(means, by = c("name1" = "names"))  %>%
    inner_join(means, by = c("name2" = "names"))  %>%
    mutate(mean1 = means.x, mean2 = means.y, diff = mean1 - mean2) %>%
    select(test, name1, name2, mean1, mean2, diff, estimate,
           sigma, tstat, pvalue, symbol)
}

#' cldGrandMean.glht
#'
#' @param obj
#' @param data
#' @param colnames
#' @param grp
#' @param Y
#'
#' @return cldGrandMean.glht
#' @export
cldGrandMean.glht <- function(obj, data, colnames, grp, Y) {
  #  e.g. "haeufigkeit" "mischung" "week"
  stopifnot(class(obj) == "glht")
  mean1 <- colGroupMeans(data, colnames, Y, add = setdiff(colnames,grp))
  mean2 <- colGroupMeans(mean1, "grpname", "means")
  means <- full_join(mean1, mean2, by = c("grpname" = "names")) %>% tbl_df
  #          grpname  names    means.x  means.y
  #   1      3:1  3:1:1   8.062564 103.9348
  #   2      3:1 3:1:10 190.199806 103.9348
  #   3      3:1 3:1:11 191.130982 103.9348

  d <- as_data_frame.glht(obj)

  d2 <- d %>% mutate(
    name1 = str_replace(name1,"C ",""),
    test = str_replace(test,"C ",""),
    name2 = delColons(name2)
  )

  #      test name1 name2  estimates      pvalues symbols
  # 1   3:1:1  3:1:1    3:1 -95.872228 6.550316e-15     ***
  # 2   3:1:2  3:1:2    3:1  86.265014 1.293299e-12     ***
  # 3   3:1:3  3:1:3    3:1  87.196190 6.521450e-13     ***

  # inner_join(d2, means, by=c("name1"="names"))  %>%
  cbind(d2,means) %>% tbl_df %>%
    select(-test,-name1,-name2) %>%
    select(names, grpname, means.x, means.y, estimate,
           sigma, tstat, pvalue, symbol) %>%
    rename(name1 = names, name2 = grpname, mean1 = means.x, mean2 = means.y) %>%
    mutate(
      diff  = mean1 - mean2,
      label = ifelse(trim(symbol) == "","",ifelse(estimate > 0,"A","B")),
      test  = paste0(name1,"- GrandMean:",name2)
    ) %>%
    select(test, name1, name2, mean1, mean2, diff, estimate,
           sigma, tstat, pvalue, symbol, label)
}

#' cld mw vergl
#'
#' @param obj
#' @param dat
#' @param name
#' @param testgr
#' @param fmwvergl
#' @param fcld
#' @param colnames
#' @param Y
#' @param pval
#' @param type
#' @param alles
#' @param ord
#'
#' @return cldmwvergl
#' @export
cldmwvergl <-
  function(obj,dat,name,testgr,fmwvergl,fcld,colnames,Y = "Y",
           pval = 0.06, type = "Tukey", alles = FALSE, ord =
             TRUE) {
    if (grepl(type,"Tukey",ignore.case = TRUE)) {
      m <- means.glht(obj, data = dat, colnames = colnames,Y = Y) %>%
        mutate(testgr = testgr, vergl = name) %T>%
        write.table(
          file = fmwvergl, sep = ";", append = TRUE, row.names = FALSE,
          col.names = !file.exists(fmwvergl)
        )

      m <- clddisplay2(
        sigdata = m,vergl = name, colnames = colnames,data = dat,
        pval = pval,alles = alles, ord = ord)

      if (nrow(m) > 0) {
        m %<>%
          mutate(
            testgr = testgr, vergl = name,
            cnames = paste(colnames,collapse = ","), # TEST
            symbol = "") %T>%
          write.table(
            file = fcld, sep = ";", append = TRUE, row.names = FALSE,
            col.names = !file.exists(fcld))
      }
    } else if (grepl(type,"GrandMean",ignore.case = TRUE)) {
      m <- cldGrandMean.glht(
        obj,data = dat,colnames = colnames,Y = Y,grp = name
      )  %>%
        mutate(testgr = testgr, vergl = name)

      m %>% select(-label) %T>%
        write.table(
          file = fmwvergl, sep = ";",append = TRUE,
          row.names = FALSE, col.names = !file.exists(fmwvergl)
        )

      m %>% mutate(names = name1, label = symbol) %>%
        select(names,label,testgr,vergl) %>%
        mutate(cnames = paste(colnames,collapse = ","), # TEST
               symbol = "") %T>% #,symbols) %T>%
        write.table(
          file = fcld, sep = ";", append = TRUE, row.names = FALSE,
          col.names = !file.exists(fcld)
        )
    }
  }

#' prepare compact letter display
#'
#' @param fname
#'
#' @return preparecld
#' @export
preparecld <- function(fname) {
  clddaten <-
    read.csv2(fname, encoding = "UTF-8", stringsAsFactors = FALSE) %>%
    filter(!names %in% c("names","-"))

  res <- clddaten %>% select(-vergl,-label) %>% distinct

  for (v in unique(clddaten$vergl)) {
    dat <- clddaten %>%
      filter(vergl == v) %>%
      select(-vergl) %>%
      rename_(.dots = setNames("label",v))

    # names(dat)[names(dat)=="labels"] <- v

    res %<>% full_join(dat)
  }
  res[is.na(res)] <- ""
  res
}



#' test if x and y are in a significant interactions
#'
#' @param x
#' @param y
#' @param cname vector of fixed values
#' @param mod   model
#'
#' @return x and y significant interact
#' @export
sign_interact <- function(x,y, cname, mod) {
  stopifnot(x != y)
  str <- paste(intersect(cnames, c(x,y)),collapse = ".*")
  xs <- grep(str,rownames(anova(mod, type = "marginal")), value = TRUE)
  for (x in xs)
    if (anova(mod, type = "marginal")[x,][["p-value"]] <= 0.05)
      return(TRUE)

  FALSE
}

#' .onAttach
#'
#' @param libname
#' @param pkgname
#'
#' @return NULL
#' @export
.onAttach <- function(libname, pkgname) {
  # options(java.parameters = "-Xmx8000m")
  options(dplyr.width = Inf)
  lattice.options(default.args = list(as.table = TRUE))
  lattice.options(skip.boundary.labels = 0)
  Sys.setenv(LANG = "en")
}


#' model.matrix.gls
#'
#' @param object
#' @param ...
#'
#' @return
#' @export
model.matrix.gls <- function(object, ...) {
  model.matrix(terms(object), data = getData(object), ...)
}

#' model.frame.gls
#'
#' @param object
#' @param ...
#'
#' @return
#' @export
model.frame.gls <- function(object, ...) {
  model.frame(formula(object), data = getData(object), ...)
}

#' terms.gls
#'
#' @param object
#' @param ...
#'
#' @return
#' @export
terms.gls <- function(object, ...) { terms(model.frame(object), ...) }


# https://github.com/ckluss/cldtest

# https://stat545-ubc.github.io/packages03_activity_part2.html
# Tools > Shell
# We will add GitHub repository as a remote, using traditional nickname origin:
# git remote add origin https://github.com/ckluss/cldtest
# Push your local work, on branch master to the new origin remote and set
# this remote as the new default: git push -u origin master.
# Close the shell, you will now be able to push and pull directly from RStudio.
#
# library(devtools)
# devtools::install_github("ckluss/cldtest")
# suppressWarnings(library(cldtest))

# library("devtools")
# devtools::document()
# devtools::load_all()
# use_vignette("overview")

# devtools::install()
# devtools::build()
# devtools::install_github()
# devtools::load_all()
# ?cldtest

# Turn on the credential helper
# Windows In the shell, enter git config --global credential.helper wincred
