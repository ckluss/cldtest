

#' tagGL
#'
#' @param dat
#' @param x
#'
#' @return dat
#' @export
tagGL <- function(dat, x) {

  ptag_CF1977 <- paste0(x,"ptagGL_CF1977")
  ptag_Ralf   <- paste0(x,"ptagGL_Ralf")
  ptag        <- paste0(x,"ptag")

  f1 <- lazyeval::interp(
    ~ ifelse(hfgkt == 4,
             (z/4 + lead(z,1)/4 + lead(z,2)/4 + lead(z,3)/4)/28,
             ifelse(hfgkt == 3, (z/3 + lead(z,1)/3 + lead(z,2)/3)/21,
                    ifelse (hfgkt == 7,  z/(hfgkt*7), NA_real_))),
    z=as.name(x))

  f2 <- lazyeval::interp(
    ~ ifelse(!is.na(y), y,
             ifelse(hfgkt == 4, (z/3 + lead(z,1)/3 + lead(z,2)/3)/28,
                    ifelse(hfgkt == 3, (z/2 + lead(z,1)/2)/21, y))),
    z=as.name(x), y = as.name(ptag_CF1977))

  f3 <- lazyeval::interp(
    ~ ifelse(!is.na(y), y,
             ifelse(hfgkt == 4, (z/2 + lead(z,1)/2)/28,
                    ifelse(hfgkt == 3, z/21, y))),
    z=as.name(x), y = as.name(ptag_CF1977))

  f4 <- lazyeval::interp(~ ifelse(!is.na(y), y, ifelse(hfgkt == 4, z/28, y)),
               z=as.name(x), y = as.name(ptag_CF1977))

  f1_ralf <- lazyeval::interp(
    ~ ifelse(hfgkt %in% 3:4, (lag(z)/3 + z/3 + lead(z)/3)/(hfgkt*7),
             ifelse (hfgkt == 7,  z/(hfgkt*7), NA_real_)),
    z=as.name(x))

  f2_ralf <- lazyeval::interp(
    ~ ifelse(hfgkt %in% 3:4 & is.na(y), (z/2 + lead(z)/2)/(hfgkt*7), y),
    z=as.name(x), y = as.name(ptag_Ralf))

  f3_ralf <- lazyeval::interp(
    ~ ifelse(hfgkt %in% 3:4 & is.na(y), (lag(z)/2 +  z/2)/(hfgkt*7), y),
    z=as.name(x), y = as.name(ptag_Ralf))

  f4_ralf <- lazyeval::interp(~ ifelse(hfgkt %in% 3:4 & is.na(y), z/(hfgkt*7), y),
                    z=as.name(x), y = as.name(ptag_Ralf))

  # ftag <- lazyeval::interp(~ ifelse(hfgkt %in% 3:4, z/(hfgkt*7), NA_real_), z=as.name(x))
  ftag <- lazyeval::interp(~ z/(hfgkt*7), z=as.name(x))

  # flaeche Datum code jnr mischung interval serie beerntung wdh

  dat %>%
    # mutate(hfgkt = as.numeric(as.character(hfgkt))) %>%
    # arrange(ort, jahr, kgmix, hfgkt, wdh, artnutz, datum) %>%
    # group_by(ort, jahr, kgmix, hfgkt, wdh, artnutz) %>%
    mutate(hfgkt = as.numeric(as.character(hfgkt))) %>%
    arrange(flaeche, jahr, mischung, hfgkt, interval, wdh, Datum) %>%
    group_by(flaeche, jahr, mischung, hfgkt, interval, wdh) %>%
    mutate_(.dots = setNames(list(f1), ptag_CF1977)) %>%
    mutate_(.dots = setNames(list(f2), ptag_CF1977)) %>%
    mutate_(.dots = setNames(list(f3), ptag_CF1977)) %>%
    mutate_(.dots = setNames(list(f4), ptag_CF1977)) %>%
    mutate_(.dots = setNames(list(f1_ralf), ptag_Ralf)) %>%
    mutate_(.dots = setNames(list(f2_ralf), ptag_Ralf)) %>%
    mutate_(.dots = setNames(list(f3_ralf), ptag_Ralf)) %>%
    mutate_(.dots = setNames(list(f4_ralf), ptag_Ralf)) %>%
    mutate_(.dots = setNames(list(ftag), ptag)) %>%
    ungroup
}

